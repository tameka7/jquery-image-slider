'use strict';

$(document).ready(function () {
    $('img').slide({
        animation: 'fade',
        controlsContainer: 'img'
    });
});
